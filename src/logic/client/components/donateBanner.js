import { componentScriptLoader } from "@logic/client/clientNavigation.js";

export default function registerDonateBanner() {
  componentScriptLoader(".donate-banner", () => {
    let noDonationTime = localStorage.getItem("noDonationPopupTime");
    let donateVisible = noDonationTime ? noDonationTime < Date.now() : true;
    const bodyEl = document.querySelector("body");
    if (!donateVisible) bodyEl.classList.add("no-donation-banner");
    document.getElementById("disableDonation").onclick = () => {
      localStorage.setItem("noDonationPopupTime", Date.now() + 259200000);
      bodyEl.classList.add("no-donation-banner");
    };
  });
}

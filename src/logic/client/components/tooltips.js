import { componentScriptLoadAll } from "@logic/client/clientNavigation.js";
import tippy from "tippy.js";

export default function registerTooltips() {
  componentScriptLoadAll("[tippy]", (e) => {
    const target = document.querySelector(e.attributes.tippy.value);

    if (!!e && !!target) {
      e.classList.remove("d-none");
      tippy(target, {
        placement: e.dataset.placement || "auto",
        interactive: true,
        animation: "shift-away",
        trigger: e.dataset.trigger || "mouseenter focus",
        content: e
      });
    }
  });
}

import slugify from "@sindresorhus/slugify";

import {
  navigate,
  initNavigation,
  componentScriptLoader
} from "@logic/client/clientNavigation.js";
import { startTracking, spaNavigationTrack } from "@logic/client/mtm-loader.js";

import registerDonateBanner from "@logic/client/components/donateBanner.js";
import registerDarkModeSwitch from "@logic/client/components/darkMode.js";

import registerHeader from "@logic/client/components/header.js";
import registerHomepage from "@logic/client/components/homepage.js";
import registerSidenav from "@logic/client/components/sidenav.js";
import registerSearch from "@logic/client/components/search.js";

import registerTooltips from "@logic/client/components/tooltips.js";
import registerSidebars from "@logic/client/components/sidebars.js";
import registerStarSelectors from "@logic/client/components/starSelectors.js";

export default function registerComponents() {
  // Navigation
  let correctedPageSlug = window.location.pathname
    .split("/")
    .map((folder) => slugify(folder, { decamelize: false }))
    .join("/");
  if (correctedPageSlug != window.location.pathname)
    window.location.replace(correctedPageSlug);

  window.navigate = navigate;
  initNavigation(["main", ".ports", "body"]);

  // Components
  registerDonateBanner();
  registerDarkModeSwitch();

  registerHeader();
  registerHomepage();
  registerSidenav();
  registerSearch();

  registerSidebars();
  registerTooltips();
  registerStarSelectors();

  // Tracking code
  startTracking("https://analytics.ubports.com/", "3");
  window.addEventListener("spa:navigation", spaNavigationTrack);
  window.addEventListener("spa:navigation", () => {
    if (!!window.location.hash)
      document.querySelector(window.location.hash)?.scrollIntoView(true);
    else window.scrollTo(0, 0);
  });
}

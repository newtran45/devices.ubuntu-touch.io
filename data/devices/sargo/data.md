---
name: "Google Pixel 3a/3a XL"
comment: "community device"
description: "The Pixel 3a is available in Black, Clearly White, and Purple-ish. Armed with a Snapdragon 670 processor and 4GB of RAM, it has a good price for a Linux phone. What else? An adequate battery capacity of 3,000 mAh, vivid 5.6-inch OLED display, Type-C USB, headphone jack, and good camera quality."
deviceType: "phone"
tag: "promoted"
subforum: "69/google-pixel-3a-3a-xl"

deviceInfo:
  - id: "cpu"
    value: "Octa-core 64-bit"
  - id: "chipset"
    value: "Qualcomm SDM670 Snapdragon 670 (10 nm)"
  - id: "gpu"
    value: "Qualcomm Adreno 615"
  - id: "rom"
    value: "64GB"
  - id: "ram"
    value: "4GB"
  - id: "android"
    value: "Android 9.0"
  - id: "battery"
    value: "3000 mAh"
  - id: "display"
    value: "1080x2220 pixels, 5.6 in"
  - id: "rearCamera"
    value: "12.2MP"
  - id: "frontCamera"
    value: "8MP"

sources:
  portType: "reference"
  portPath: "android9"
  deviceGroup: "google-pixel-3a"
  deviceSource: "android_device_google_bonito"
  kernelSource: "android_kernel_google_bonito"

contributors:
  - name: "fredldotme"
    forum: "https://forums.ubports.com/user/fredldotme"
    photo: "https://forums.ubports.com/assets/uploads/profile/2070-profileavatar.png"
---

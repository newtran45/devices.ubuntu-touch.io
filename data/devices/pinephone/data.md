---
name: "Pinephone"
deviceType: "phone"
buyLink: "https://pine64.com/"
price:
  avg: 150
  currency: "USD"
subforum: "88/pinephone"
description: "The PinePhone is a smartphone created by PINE64. It is capable of running mainline Linux and is supported by many partner projects. Various community editions have been released for Linux operating systems, including Ubuntu Touch."

deviceInfo:
  - id: "cpu"
    value: "4x 1152 MHz Cortex-A53"
  - id: "chipset"
    value: "Allwinner A64"
  - id: "gpu"
    value: "Mali-400 MP2"
  - id: "rom"
    value: "16 GB / 32 GB"
  - id: "ram"
    value: "2 GB / 3 GB"
  - id: "battery"
    value: "3000 mAh"
  - id: "display"
    value: '5.95" 720x1440 IPS'
  - id: "rearCamera"
    value: "Single 5MP, 1/4″, LED Flash"
  - id: "frontCamera"
    value: "Single 2MP, f/2.8, 1/5″"
  - id: "arch"
    value: "arm64"
  - id: "dimensions"
    value: "160.5mm x 76.6mm x 9.2mm"
  - id: "weight"
    value: "185 grams"
  - id: "releaseDate"
    value: "05.09.2019"

sources:
  #portType: "community"
  #deviceSource: "pinephone"
  portType: "external"
  issuesLink: "https://gitlab.com/ook37/pinephone-pro-debos/-/issues"

externalLinks:
  - name: "Xenial Builds"
    link: "https://ci.ubports.com/job/rootfs/job/rootfs-pinephone-systemimage/"
  - name: "Focal Builds"
    link: "https://gitlab.com/ook37/pinephone-pro-debos/-/releases"
  - name: "Install instructions"
    link: "https://ubports.com/blog/ubports-news-1/post/pinephone-and-pinephone-pro-3889"

communityHelp:
  - name: "Ubuntu Touch subforum on Pine64 Forum"
    link: "https://forum.pine64.org/forumdisplay.php?fid=125"
  - name: "Pinephone subreddit"
    link: "https://www.reddit.com/r/pinephone/"
  - name: "IRC ( #pinephone on irc.pine64.org )"
    link: "https://www.pine64.org/web-irc/"
  - name: "#pinephone on Pine64 Discord"
    link: "https://discord.com/invite/DgB7kzr"
  - name: "#pinephone:matrix.org"
    link: "https://matrix.to/#/#pinephone:matrix.org"
  - name: "Telegram chat"
    link: "https://t.me/utonpine"

contributors:
  - name: "Pine64"
    role: "Phone maker"
  - name: "Oren Klopfer"
    role: "Developer"
---

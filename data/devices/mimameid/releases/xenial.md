---
portType: "Halium 11.0"

portStatus:
  - categoryName: "Actors"
    features:
      - id: "manualBrightness"
        value: "+"
      - id: "notificationLed"
        value: "+-"
        bugTracker: "https://github.com/HelloVolla/ubuntu-touch-beta-tests/issues/138" # works as "charging" indicator but doesn't blink on notifications
      - id: "torchlight"
        value: "+"
      - id: "vibration"
        value: "+"
        bugTracker: "https://github.com/HelloVolla/ubuntu-touch-beta-tests/issues/134" # not on alarms + weak osk feedback
  - categoryName: "Camera"
    features:
      - id: "flashlight"
        value: "+"
      - id: "photo"
        value: "+"
      - id: "video"
        value: "+"
        bugTracker: "https://github.com/HelloVolla/ubuntu-touch-beta-tests/issues/145" # 1440p recording not possible
      - id: "switchCamera"
        value: "+"
  - categoryName: "Cellular"
    features:
      - id: "carrierInfo"
        value: "+"
      - id: "dataConnection"
        value: "+"
        bugTracker: "https://github.com/HelloVolla/ubuntu-touch-beta-tests/issues/133" # sometimes stays disconnected after standby/sleep
      - id: "dualSim"
        value: "+"
      - id: "calls"
        value: "+"
      - id: "mms"
        value: "+"
      - id: "pinUnlock"
        value: "+"
      - id: "sms"
        value: "+"
      - id: "audioRoutings"
        value: "+"
      - id: "voiceCall"
        value: "+"
      - id: "volumeControl"
        value: "+-"
        bugTracker: "https://github.com/HelloVolla/ubuntu-touch-beta-tests/issues/146" # doesn't work on BT headphones
  - categoryName: "Endurance"
    features:
      - id: "batteryLifetimeTest"
        value: "+"
      - id: "noRebootTest"
        value: "+-"
        bugTracker: "https://github.com/HelloVolla/ubuntu-touch-beta-tests/issues/135" # toggling Wi-Fi too fast can crash device
  - categoryName: "GPU"
    features:
      - id: "uiBoot"
        value: "+"
      - id: "videoAcceleration"
        value: "+"
  - categoryName: "Misc"
    features:
      - id: "anboxPatches"
        value: "x"
      - id: "apparmorPatches"
        value: "+"
      - id: "batteryPercentage"
        value: "+"
      - id: "offlineCharging"
        value: "+" # only on devel channel currently
      - id: "onlineCharging"
        value: "+"
      - id: "recoveryImage"
        value: "+"
      - id: "factoryReset"
        value: "+-"
        bugTracker: "https://github.com/HelloVolla/ubuntu-touch-beta-tests/issues/148" # blocked on rm -rf /data/android-data, "Format data/factory reset" in UBports recovery itself works
      - id: "rtcTime"
        value: "+"
      - id: "sdCard"
        value: "+"
      - id: "shutdown"
        value: "+"
      - id: "wirelessCharging"
        value: "+"
      - id: "wirelessExternalMonitor"
        value: "-"
        bugTracker: "https://github.com/HelloVolla/ubuntu-touch-beta-tests/issues/92" # aethercast not working
      - id: "waydroid"
        value: "+-"
        bugTracker: "https://github.com/HelloVolla/ubuntu-touch-beta-tests/issues/132" # HALIUM 11 vendor images not yet available

  - categoryName: "Network"
    features:
      - id: "bluetooth"
        value: "+"
      - id: "flightMode"
        value: "+"
      - id: "hotspot"
        value: "+"
        overrideGlobal: true
      - id: "nfc"
        value: "+"
      - id: "wifi"
        value: "+"
        bugTracker: "https://github.com/HelloVolla/ubuntu-touch-beta-tests/issues/133" # sometimes stays disconnected after standby/sleep
      - id: "fmRadio"
        value: "x"
  - categoryName: "Sensors"
    features:
      - id: "autoBrightness"
        value: "+" # only on devel channel currently
      - id: "fingerprint"
        value: "+" # doesn't wake up device, power button press required first
      - id: "gps"
        value: "+"
      - id: "proximity"
        value: "+"
      - id: "rotation"
        value: "+"
      - id: "touchscreen"
        value: "+"
  - categoryName: "Sound"
    features:
      - id: "earphones"
        value: "+"
      - id: "loudspeaker"
        value: "+"
      - id: "microphone"
        value: "+"
      - id: "volumeControl"
        value: "+"
  - categoryName: "USB"
    features:
      - id: "mtp"
        value: "+"
      - id: "adb"
        value: "+"
      - id: "wiredExternalMonitor"
        value: "x"
---

---
name: "Xiaomi Redmi 4X"
comment: "wip"
deviceType: "phone"
image: "https://wiki.lineageos.org/images/devices/santoni.png"
subforum: "94/xiaomi-redmi-4-4x"

deviceInfo:
  - id: "cpu"
    value: "Octa-core 1.4 GHz Cortex-A53"
  - id: "chipset"
    value: "Qualcomm MSM8940 Snapdragon 435"
  - id: "gpu"
    value: "Adreno 505"
  - id: "rom"
    value: "16 GB / 32 GB / 64GB"
  - id: "ram"
    value: "2 GB / 3 GB / 4 GB"
  - id: "android"
    value: "MIUI based on 6"
  - id: "battery"
    value: "4100 mAh"
  - id: "display"
    value: "720x1280"
  - id: "rearCamera"
    value: "13 MP, LED flash"
  - id: "frontCamera"
    value: "5 MP, No flash"
  - id: "arch"
    value: "Arm v8"
  - id: "dimensions"
    value: "139.2 mm (5.48 in) (h), 70 mm (2.76 in) (w), 8.7 mm (0.34 in) (d)"
  - id: "weight"
    value: "150g"
  - id: "releaseDate"
    value: "14.02.2017"

contributors:
  - name: "Danct12"
    forum: "https://forums.ubports.com/user/danct12"
    role: "Maintainer"
  - name: "Shipa_2"
    forum: "https://forums.ubports.com/user/Shipa_2"
    role: "Tester"

sources:
  portType: "external"

externalLinks:
  - name: "Forum Post"
    link: "https://forums.ubports.com/topic/3682/xiaomi-redmi-4x-santoni"
  - name: "Source repos"
    link: "https://github.com/ubports-santoni"
  - name: "CI builds"
    link: "https://github.com/ubports-santoni/ubports-ci"
---

---
name: "Sony Xperia X (F5121 & F5122)"
deviceType: "phone"
tag: "promoted"
subforum: "82/sony-xperia-x-f5121-f5122"

deviceInfo:
  - id: "cpu"
    value: "Hexa-core 64-bit"
  - id: "chipset"
    value: "Qualcomm MSM8956 Snapdragon 650"
  - id: "gpu"
    value: "Qualcomm Adreno 510"
  - id: "rom"
    value: "32/64GB"
  - id: "ram"
    value: "3GB"
  - id: "android"
    value: "Android 6.0.1"
  - id: "battery"
    value: "2620 mAh"
  - id: "display"
    value: "1080x1920 pixels, 5.5 in"
  - id: "rearCamera"
    value: "23MP"
  - id: "frontCamera"
    value: "13MP"
sources:
  portType: "external"
externalLinks:
  - name: "Device source"
    link: "https://github.com/fredldotme/device-sony-suzu"
contributors:
  - name: "fredldotme"
    forum: "https://forums.ubports.com/user/fredldotme"
    photo: "https://forums.ubports.com/assets/uploads/profile/2070-profileavatar.png"
---

---
name: "Oneplus One"
deviceType: "phone"
description: "The OnePlus One has been one of the most iconic smartphones of 2014. The specs are certainly good enough with a 5.5inch 1080p display, Snapdragon 801 SoC, and good battery life. Admittedly, this is a bit of a leap from Android or Apple for a new user, but the phone flows once you get a feel for it. Keep in mind that Waydroid will never work because of its old kernel, use Anbox instead."
tag: "promoted"
subforum: "50/oneplus-one"

deviceInfo:
  - id: "cpu"
    value: "Quad-core 2.5 GHz Krait 400"
  - id: "chipset"
    value: "Qualcomm Snapdragon 801 MSM8974AC"
  - id: "gpu"
    value: "Qualcomm Adreno 330"
  - id: "rom"
    value: "16/64GB"
  - id: "ram"
    value: "2/3GB"
  - id: "android"
    value: "CyanogenMod 13"
  - id: "battery"
    value: "3100 mAh"
  - id: "display"
    value: "1080x1920 pixels, 5.5 in"
  - id: "rearCamera"
    value: "13MP"
  - id: "frontCamera"
    value: "5MP"

seo:
  description: "Flash your OnePlus One smartphone to Ubuntu Touch as your daily driver, a privacy focused OS."
  keywords: "Ubuntu Touch, OnePlus One, linux for smartphone, Linux on Phone"
---

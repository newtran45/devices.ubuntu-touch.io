---
name: "BQ Aquaris U Plus"
deviceType: "phone"

deviceInfo:
  - id: "cpu"
    value: "Octa-core Cortex-A53 1.4 GHz"
  - id: "chipset"
    value: "Qualcomm MSM8937 Snapdragon 430"
  - id: "gpu"
    value: "Qualcomm Adreno 505"
  - id: "rom"
    value: "16/32GB"
  - id: "ram"
    value: "2/3GB"
  - id: "android"
    value: "Android 7.1 Nougat"
  - id: "battery"
    value: "Removable Li-Ion 3.300 mAh"
  - id: "display"
    value: "127 mm (5.0 in) 1280x720 (~294 PPI) IPS LCD, 16M colors"
  - id: "rearCamera"
    value: "16 MP, Dual LED (flash)"
  - id: "frontCamera"
    value: "5 MP (No flash)"

contributors:
  - name: "Guf"
    forum: "https://forums.ubports.com/user/guf"

sources:
  portType: "external"

externalLinks:
  - name: "Device Subforum"
    link: "https://forums.ubports.com/topic/2941/porting-ubuntu-touch-ubports-to-tenshi-bq-aquaris-u-plus"
  - name: "Kernel sources"
    link: "http://github.com/Halium/android_kernel_bq_msm8937"
  - name: "Device sources"
    link: "http://github.com/Halium/android_device_bq_tenshi"
---

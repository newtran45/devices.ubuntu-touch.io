---
name: "Xiaomi Mi A2"
deviceType: "phone"
description: "The Xiaomi Mi A2 is a mid-tier android one smartphone released in July 2018. This budget device has a modern configuration for handling all the things you want to use Ubuntu Touch. It also has a high-end phone with a stellar battery, an excellent camera, and an advanced chipset. The screen on the 1080p HD is 5.99 inches with an 18:9 aspect ratio display. The Mi is powered by a Qualcomm Snapdragon 660 and 4GB of RAM and 64GB of storage. It has a fingerprint sensor and 12MP and 20MP dual rear cameras, and a 20MP front camera."
subforum: "92/xiaomi-mi-a2"

deviceInfo:
  - id: "cpu"
    value: "Octa-core (4x2.2 GHz Kryo 260 Gold & 4x1.8 GHz Kryo 260 Silver)"
  - id: "chipset"
    value: "Qualcomm SDM660 Snapdragon 660"
  - id: "gpu"
    value: "Adreno 512"
  - id: "rom"
    value: "32 GB, 64 GB, 128 GB"
  - id: "ram"
    value: "4 GB, 6 GB"
  - id: "android"
    value: "Android 8.1 (Oreo), upgradable to Android 10.0"
  - id: "battery"
    value: "Li-Po 3000 mAh, non-removable"
  - id: "display"
    value: "5.99 inches, 92.6 cm2 (~77.4% screen-to-body ratio), 1080 x 2160 pixels, 18:9 ratio (~403 ppi density)"
  - id: "rearCamera"
    value: '12 MP, f/1.8, 1/2.9", 1.25µm, 20 MP, f/1.8, 1/2.8", 1.0µm, PDAF, 4K@30fps, 1080p@30fps (gyro-EIS), 720p@120fps'
  - id: "frontCamera"
    value: '20 MP, f/2.2, (wide), 1/3", 0.9µm, 1080p@30fps'
  - id: "arch"
    value: "arm64"
  - id: "dimensions"
    value: "158.7 x 75.4 x 7.3 mm (6.25 x 2.97 x 0.29 in)"
  - id: "weight"
    value: "166 g (5.86 oz)"

contributors:
  - name: "Shouko"
    photo: "https://avatars.githubusercontent.com/u/17274550"
    forum: "https://forums.ubports.com/user/shouko"

sources:
  portType: "external"
  issuesLink: "https://gitlab.com/ubports/porting/community-ports/android9/xiaomi-mi-a2/xiaomi-jasmine_sprout/-/issues"

externalLinks:
  - name: "Device Repository"
    link: "https://gitlab.com/ubports/community-ports/android9/xiaomi-mi-a2/xiaomi-jasmine_sprout"
  - name: "Kernel Repository"
    link: "https://github.com/ubports-xiaomi-sdm660/android_kernel_xiaomi_sdm660"
  - name: "CI Builds"
    link: "https://gitlab.com/ubports/community-ports/android9/xiaomi-mi-a2/xiaomi-jasmine_sprout/-/pipelines"

communityHelp:
  - name: "Telegram Group"
    link: "https://t.me/shoukolab"
seo:
  description: "Switch your Xiaomi Mi A2 to Ubuntu Touch, as your open source daily driver OS."
  keywords: "Ubuntu Touch, Xiaomi Mi A2, jasmine, jasmine_sprout, Linux on Mobile"
---

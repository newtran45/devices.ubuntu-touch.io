---
name: "LG G6 (International h870) "
deviceType: "phone"

deviceInfo:
  - id: "cpu"
    value: "Octa-core Cortex-A53 & Cortex-A574 x 2.0 Ghz + 4 x 1.5 Ghz"
  - id: "chipset"
    value: "Qualcomm MSM8996 Snapdragon 821"
  - id: "gpu"
    value: "Qualcomm Adreno 430"
  - id: "rom"
    value: "32GB"
  - id: "ram"
    value: "4GB"
  - id: "android"
    value: "Android 7.0 Nougat"
  - id: "battery"
    value: "Non-removable Li-Ion 3.300 mAh"
  - id: "display"
    value: "5.7 in (14.47cm), Fullvision Quad HD+ (2880x1440) 18:9"
  - id: "rearCamera"
    value: "13 MP F/1.8 LED flash"
  - id: "frontCamera"
    value: "5MP f2.2"

contributors:
  - name: "Guf"
    forum: "https://forums.ubports.com/user/guf"

sources:
  portType: "external"

externalLinks:
  - name: "Device Subforum"
    link: "https://forums.ubports.com/topic/3729/ubuntu-touch-on-lg-g6-h870-model-call-for-testers"
  - name: "Sources"
    link: "https://github.com/LG-G6-DEV"
  - name: "Kernel sources"
    link: "https://github.com/rymdllama/android_kernel_lge_msm8996"
  - name: "Device sources"
    link: "https://github.com/rymdllama/android_device_lge_h870"
---

---
name: "Sony Xperia Z1 (C610X)"
comment: "community device"
deviceType: "phone"

deviceInfo:
  - id: "cpu"
    value: "Quad-core 2.2GHz Krait 400"
  - id: "chipset"
    value: "Qualcomm MSM8974 Snapdragon 800"
  - id: "gpu"
    value: "Adreno 330"
  - id: "rom"
    value: "16 GB"
  - id: "ram"
    value: "2 GB"
  - id: "display"
    value: "1080x1920"
  - id: "rearCamera"
    value: "20,7 MP, LED flash"
  - id: "frontCamera"
    value: "2 MP, No flash"
  - id: "arch"
    value: "armv7"
  - id: "dimensions"
    value: "144 mm (5.67 in) (h), 74 mm (2.91 in) (w), 8.5 (0.33 in) (d)"

contributors:
  - name: "konradybcio"
    role: "Maintainer"

sources:
  portType: "external"

externalLinks:
  - name: "Source repos"
    link: "https://github.com/rhine-dev"
  - name: "CI builds"
    link: "https://github.com/rhine-dev/ubports-ci"
---

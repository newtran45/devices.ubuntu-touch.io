---
name: "Lenovo Tab M10 X605F/L"
deviceType: "tablet"
description: "The Lenovo Tablet TB-X605 was designed for multimedia purpose and is equipped with a good display and audio-module. There is also a dockingstation available, which can be used as bluetooth speaker and for charging the device."
subforum: "111/lenovo-tab-m10-x605f-l"
price:
  avg: 150
disableBuyLink: true

deviceInfo:
  - id: "cpu"
    value: "Octa-core 1.8 GHz Cortex-A53"
  - id: "chipset"
    value: "Qualcomm SDM450 Snapdragon 450 (14 nm)"
  - id: "gpu"
    value: "Adreno 506"
  - id: "rom"
    value: "32 GB"
  - id: "ram"
    value: "3 GB"
  - id: "android"
    value: "9.0 (Pie)"
  - id: "battery"
    value: "4850 mAh"
  - id: "display"
    value: "256.54 mm (10.1 in) : 1920x1080 (224 PPI) - LCD IPS Touchscreen"
  - id: "rearCamera"
    value: "5 MP, No flash"
  - id: "frontCamera"
    value: "2 MP, No flash"
  - id: "arch"
    value: "arm64"
  - id: "dimensions"
    value: "242 mm (9.53 in) (w), 168 mm (6.61 in) (h), 8.1 mm (0.32 in) (d)"
  - id: "weight"
    value: "480 g"
  - id: "releaseDate"
    value: "2018"

contributors:
  - name: "Luksus"
    forum: "https://forums.ubports.com/user/luksus"
    photo: "https://avatars.githubusercontent.com/u/6659488?v=4"

sources:
  portType: "community"
  portPath: "android9"
  deviceGroup: "lenovo-tab-m10-fhd"
  deviceSource: "lenovo-x605"
  kernelSource: "kernel-lenovo-x605"
---

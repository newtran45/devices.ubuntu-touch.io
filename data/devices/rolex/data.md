---
name: "Xiaomi Redmi 4A"
comment: "wip"
deviceType: "phone"
image: "http://cdn2.gsmarena.com/vv/pics/xiaomi/xiaomi-redmi-4a-3.jpg"

deviceInfo:
  - id: "cpu"
    value: "Quad-core 1.4 GHz Cortex-A53"
  - id: "chipset"
    value: "Qualcomm MSM8917 Snapdragon 425"
  - id: "gpu"
    value: "Adreno 308"
  - id: "rom"
    value: "16 GB / 32 GB"
  - id: "ram"
    value: "2 GB / 3 GB"
  - id: "display"
    value: "720x1280"
  - id: "rearCamera"
    value: "13 MP, LED flash"
  - id: "frontCamera"
    value: "5 MP, No flash"
  - id: "arch"
    value: "arm64"
  - id: "dimensions"
    value: "139.9 x 70.4 x 8.5 mm"

contributors:
  - name: "TEA"
    role: "Maintainer"

sources:
  portType: "external"

externalLinks:
  - name: "Forum Post"
    link: "https://forum.xda-developers.com/redmi-4a/development/ubuntu-touch-rolex-redmi4a-t4058619"
  - name: "Source repos"
    link: "https://github.com/areyoudeveloper1"
  - name: "CI builds"
    link: "https://github.com/areyoudeveloper1/ubports-ci"
---

---
name: "Google Nexus 5"
deviceType: "phone"
tag: "promoted"
price:
  avg: 50
subforum: "56/google-nexus-5"

deviceInfo:
  - id: "cpu"
    value: "Krait 400, 2300MHz, 4 Cores"
  - id: "chipset"
    value: "Qualcomm Snapdragon 800, MSM8974AA"
  - id: "gpu"
    value: "Qualcomm Adreno 330, 450MHz, 4 Cores"
  - id: "rom"
    value: "16/32GB"
  - id: "ram"
    value: "2GB, 800MHz"
  - id: "android"
    value: "4.4.3"
  - id: "battery"
    value: "2300 mAh"
  - id: "display"
    value: "1080x1920 pixels, 5 in"
  - id: "rearCamera"
    value: "8MP"
  - id: "frontCamera"
    value: "1.2MP"
contributors:
  - name: Rúben Carneiro
    forum: https://forums.ubports.com/user/rubencarneiro
    photo: https://forums.ubports.com/assets/uploads/profile/186-profileavatar.png
  - name: Flohack
    forum: https://forums.ubports.com/user/flohack
    photo: https://forums.ubports.com/assets/uploads/profile/414-profileavatar.png
seo:
  description: "Flash your Nexus 5 smartphone with the latest version of Ubuntu Touch operating system, a privacy focus OS developed by hundreds of people."
  keywords: "Nexus 5, Privacy Focus, Linux Phone"
---

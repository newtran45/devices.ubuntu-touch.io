---
name: "Pinetab"
deviceType: "tablet"
buyLink: "https://pine64.com/"

deviceInfo:
  - id: "cpu"
    value: "4x 1152 MHz Cortex-A53"
  - id: "chipset"
    value: "Allwinner A64"
  - id: "gpu"
    value: "Mali-400 MP2"
  - id: "rom"
    value: "64 GB"
  - id: "ram"
    value: "2 GB"
  - id: "battery"
    value: "6000 mAh"
  - id: "display"
    value: '10.1" 1280×800 IPS'
  - id: "rearCamera"
    value: "Single 5MP, 1/4″, LED Flash"
  - id: "frontCamera"
    value: "Single 2MP, f/2.8, 1/5″"
  - id: "arch"
    value: "arm64"
  - id: "dimensions"
    value: "258mm x 170mm x 11.2mm"
  - id: "weight"
    value: "575 grams"

sources:
  portType: "community"
  deviceSource: "pinephone"

externalLinks:
  - name: "Builds"
    link: "https://ci.ubports.com/job/rootfs/job/rootfs-pinetab-systemimage/"

communityHelp:
  - name: "Pine64 subforum on UBports Forum"
    link: "https://forums.ubports.com/category/87/pine64"
  - name: "Pinetab subforum on Pine64 Forum"
    link: "https://forum.pine64.org/forumdisplay.php?fid=142"
  - name: "PineTab subreddit"
    link: "https://www.reddit.com/r/PineTab/"
  - name: "IRC ( #pinetab on irc.pine64.org )"
    link: "https://www.pine64.org/web-irc/"
  - name: "#pinetab on Pine64 Discord"
    link: "https://discord.gg/knRjbFe2jP"
  - name: "Telegram chat"
    link: "https://t.me/utonpine"

contributors:
  - name: "Pine64"
    role: "Phone maker"
---
